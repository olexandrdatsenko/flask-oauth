from mongoengine import StringField, EmailField, BooleanField, ListField, ReferenceField

from flask_login import UserMixin
from flask import jsonify

from social_flask_mongoengine.models import FlaskStorage

from example import db
import pprint

from bson.json_util import dumps, CANONICAL_JSON_OPTIONS, RELAXED_JSON_OPTIONS
from bson.objectid import ObjectId
import json
# from bson import json_util

class User(db.Document, UserMixin):
    username = StringField(max_length=200)
    password = StringField(max_length=200, default='')
    name = StringField(max_length=100)
    email = EmailField()
    active = BooleanField(default=True)

    @property
    def social_auth(self):
        return FlaskStorage.user.objects(user=self)

    def is_active(self):
        return self.active
    
    # @queryset_manager
    # def get_user_by_id(user_id):
    #     return queryset.filter(id=user_id)

    def get_user(user_id):
        # user_id = ObjectId(user_id)
        # print(user_id, type(user_id))
        user = User.objects.get(id=user_id)
        output = {}
        if isinstance(user.id, ObjectId):
            user.id = str(user.id)
            output = {'name': user.username, 'id': user.id, 'email': user.email}
        return jsonify(output)


    def get_list_users():
        output = []
        for user in User.objects:
            pprint.pprint(user.to_mongo())
            print(user.to_mongo().to_dict())
            first_user = user 
            output.append({'name': user.username, 'id': str(user.id), 'email': user.email})

        return jsonify({'result': output})


