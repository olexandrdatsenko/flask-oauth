import connexion

from flask_script import Server, Manager, Shell
from example import app, db

app_con = connexion.App(__name__, specification_dir='./')
app_con.add_api('swagger.yml')

manager = Manager(app)
manager.add_command('runserver', Server())
manager.add_command('shell', Shell(make_context=lambda: {
    'app': app,
    'db': db
}))



if __name__ == '__main__':
    # manager.run()
    app_con.run()
